<div class="row">
	<div class="col-sm-12">
		@foreach($locations as $key => $value)
		<div class="box box-solid">
			<div class="box-header">
	            <h3 class="box-title">@lang('sale.location'): {{$value}}</h3>
	        </div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-condensed table-bordered table-th-green text-center table-striped add_opening_stock_table">
								<thead>
								<tr>
									<th>@lang( 'product.product_name' )</th>
									<th>Available</th>
									<th>Damaged</th>
									
									<th>Total Damaged</th>
									
								</tr>
								</thead>
								<tbody>
@php
	$subtotal = 0;
@endphp
@foreach($product->variations as $variation)
	@if(empty($purchases[$key][$variation->id]))
		@php
			$purchases[$key][$variation->id][] = ['quantity' => 0, 
			'purchase_price' => $variation->default_purchase_price,
			'purchase_line_id' => null,
			'lot_number' => null
			]
		@endphp
	@endif

@foreach($purchases[$key][$variation->id] as $sub_key => $var)
	@php

	$purchase_line_id = $var['purchase_line_id'];

	$qty = $var['quantity'];

	$purcahse_price = $var['purchase_price'];
    $damaged = $var['damaged'];
	$row_total = $qty * $purcahse_price;

	$subtotal += $row_total;
	$lot_number = $var['lot_number'];

	$remaining=$qty- $damaged;
	
	@endphp

<tr>
	<td>
		 {{ $product->name }} @if( $product->type == 'variable' ) (<b>{{ $variation->product_variation->name }}</b> : {{ $variation->name }}) @endif

		@if(!empty($purchase_line_id))
			{!! Form::hidden('stocks[' . $key . '][' . $variation->id . '][' . $sub_key . '][purchase_line_id]', $purchase_line_id); !!}
		@endif
	</td>
	<td>

		<div class="input-group">
		  {!! Form::text('stocks[' . $key . '][' . $variation->id . '][' . $sub_key . '][quantity]', @format_quantity($qty) , ['class' => 'form-control input-sm input_number purchase_quantity input_quantity', 'required']); !!}
		  <span class="input-group-addon">
		    {{ $product->unit->short_name }}
		  </span>
		</div>
	</td>
<td>
	 {!! Form::text('stocks[' . $key . '][' . $variation->id . '][' . $sub_key . '][damage]', '', ['class' => 'form-control input-sm input_number damage', 'required']); !!}

	 	{!! Form::hidden('stocks[' . $key . '][' . $variation->id . '][' . $sub_key . '][purchase_price]', @num_format($purcahse_price) , ['class' => 'form-control input-sm input_number ', 'required']); !!}


	 		 {!! Form::hidden('stocks[' . $key . '][' . $variation->id . '][' . $sub_key . '][damaged]', $damaged, ['class' => 'form-control input-sm input_number damaged', 'required']); !!}
</td>




	<td>
		<span class="row_subtotal_before_tax">{{@num_format($damaged)}}</span>
	</td>
	
			</tr>
		@endforeach
	@endforeach
								</tbody>
								<tfoot>

								</tfoot>
						</table>
						
					</div>
				</div>
			</div>
		</div> <!--box end-->
		@endforeach
	</div>
</div>